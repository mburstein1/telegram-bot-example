Feature: Movie

    Scenario 01: Get info for one Movie
        Given I access the bot page
        When I ask for "/awards Titanic"
        Then I should see the awards of that movie "The movie Titanic Won 11 Oscars. 126 wins & 83 nominations total"

    Scenario 02: Get info for two Movies
        Given I access the bot page
        When I ask for "/awards Titanic Gigi"
        Then I should see the awards of that movie "The movie Titanic Won 11 Oscars. 126 wins & 83 nominations total, The movie Gigi Won 9 Oscars. 21 wins & 9 nominations total"

    Scenario 03: Ask for a movie that did not win an award
        Given I access the bot page
        When I ask for "/awards Terminator"
        Then I should see the message "The movie Terminator has no awards"

    Scenario 03: Ask for a movie that does not exist
        Given I access the bot page
        When I ask for "/awards Noexisteestapelicula"
        Then I should see the message "The movie Noexisteestapelicula does not exist"
