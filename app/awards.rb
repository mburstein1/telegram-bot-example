class Awards
  def initialize(movie_name)
    @movie_name = movie_name
  end

  def get_awards(api_key, api_url)
    response = Faraday.get(api_url, { t: @movie_name, apikey: api_key })
    response_json = JSON.parse(response.body)
    if movie_exists?(response_json)
      if response_json['Awards'] == 'N/A'
        'has no awards'
      else
        response_json['Awards']
      end
    else
      'does not exist'
    end
  end

  private

  def movie_exists?(response_json)
    response_json['Error'].nil?
  end
end
