require 'spec_helper'
require 'web_mock'

describe 'Awards' do
  let(:api_key) { 'your_api_key' }
  let(:api_url) { 'http://www.omdbapi.com/' }

  context 'when the movie exists' do
    let(:api_ombd_stub_body) do
      {
        "Title": 'Titanic',
        "Awards": 'Won 11 Oscars. 126 wins & 83 nominations total'
      }.to_json
    end

    before(:each) do
      stub_request(:get, "#{api_url}?apikey=#{api_key}&t=Titanic")
        .to_return(status: 200, body: api_ombd_stub_body)
    end

    it 'should return awards for a movie' do
      awards = Awards.new('Titanic').get_awards(api_key, api_url)
      expect(awards).to eq(JSON.parse(api_ombd_stub_body)['Awards'])
    end
  end

  context 'when the movie does not exist' do
    let(:non_existent_movie) { 'NonExistentMovie' }
    let(:non_existent_movie_response) do
      {
        "Response": 'False',
        "Error": 'Movie not found!'
      }.to_json
    end

    before(:each) do
      stub_request(:get, "#{api_url}?apikey=#{api_key}&t=#{non_existent_movie}")
        .to_return(status: 200, body: non_existent_movie_response)
    end

    it 'returns a message when the movie does not exist' do
      awards = Awards.new(non_existent_movie).get_awards(api_key, api_url)
      expect(awards).to eq('does not exist')
    end
  end

  context 'when the movie has no awards' do
    let(:movie_without_awards) { 'MovieWithoutAwards' }
    let(:movie_without_awards_response) do
      {
        "Title": 'MovieWithoutAwards',
        "Awards": 'N/A',
        "Response": 'True'
      }.to_json
    end

    before(:each) do
      stub_request(:get, "#{api_url}?apikey=#{api_key}&t=#{movie_without_awards}")
        .to_return(status: 200, body: movie_without_awards_response)
    end

    it 'returns a message when the movie has no awards' do
      awards = Awards.new(movie_without_awards).get_awards(api_key, api_url)
      expect(awards).to eq('has no awards')
    end
  end
end
